import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pigpio


def client_exit():
    set_fan_rpm(0)
    set_servo_pos(1350)
    # exit i2c and gpio
    pi.i2c_close(enc_tmp)
    pi.i2c_close(amb_tmp)
    pi.stop()
    exit()


def set_fan_rpm(dc):
    pi.set_PWM_frequency(fan_pin, fan_frequency)
    # duty cycle range is 0-255
    pi.set_PWM_dutycycle(fan_pin, dc)


def temp_calc(a):
    # calculation of the temperature based on
    # the first word and the first byte

    byte0 = pi.i2c_read_byte_data(a, 0)
    word0 = pi.i2c_read_word_data(a, 0)
    l4b = (word0 & 0b1111000000000000) >> 12
    temperature = ((byte0 << 4) | l4b) * 0.0625
    if temperature > 0x7FF:
        temperature = temperature - 4096
    temp_f = temperature * 9 / 5 + 32
    return temp_f


def set_servo_pos(pulsewidth):
    # Pulsewidth between 0 and 2500 out of bounds may damage the servo
    # Full Close = 1350 open is 2300
    pi.set_servo_pulsewidth(servo_pin, pulsewidth)
    pi.set_servo_pulsewidth(servo_pin_2, pulsewidth)


def integrate(i, e1, e2, ts):
    area = ts * (e2 + e1) / 2

    integral = i + area

    return integral



# information needed for fan control
fan_pin = 12
fan_frequency = 25000
fan_dutycycle = 0

# information for servo control
servo_pin = 19
servo_frequency = 50
servo_dutycycle = 0

servo_pin_2 = 26

pi = pigpio.pi()
pi.set_mode(fan_pin, pigpio.OUTPUT)
pi.set_mode(servo_pin, pigpio.OUTPUT)
pi.set_mode(servo_pin_2, pigpio.OUTPUT)

# initialize the fan and set the speed(dutycycle) to zero
pi.set_PWM_frequency(fan_pin, fan_frequency)
pi.set_PWM_dutycycle(fan_pin, fan_dutycycle)

# initialize the servo and set it to default position
pi.set_PWM_frequency(servo_pin, servo_frequency)
pi.set_servo_pulsewidth(servo_pin, 0)

pi.set_PWM_frequency(servo_pin_2, servo_frequency)
pi.set_servo_pulsewidth(servo_pin_2, 0)

# configuration for the temperature sensors
# i2c bus of the Raspberry Pi 3
i2c_bus = 1
# TMP 102 address on the i2c bus
addr1 = 0x49
addr2 = 0x48
enc_tmp = pi.i2c_open(i2c_bus, addr1, 0)
amb_tmp = pi.i2c_open(i2c_bus, addr2, 0)

K_p = 10
K_i = 1
K_d = 1

setTemp = 80
timeStep = 1000 #milliseconds
I_int = 0

# Create figure for plotting
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
xs = []
ys = []

previousError = 1
integral = 1
slope = 1
fanSpeed = 0
errorIntegral = 0
reachedTemp = 0

def PID ():
    global reachedTemp
    global errorIntegral
    global previousError
    # enc_tmp = pi.i2c_open(i2c_bus, addr1, 0)
    # amb_tmp = pi.i2c_open(i2c_bus, addr2, 0)
    enclosureTemp = int(temp_calc(enc_tmp))
    print(enclosureTemp)
    ambientTemp = int(temp_calc(amb_tmp))
    print(ambientTemp)
    setTemp = ambientTemp + 10
    error = enclosureTemp - setTemp

    if error == 0 and reachedTemp == 0:
        reachedTemp = 1

        # if fanSpeed < 255:
    errorIntegral = integrate(errorIntegral, previousError, error, timeStep)

    if reachedTemp == 1:
        # clear error once we reach the temp for the first time
        errorIntegral = 0
        reachedTemp = 2

    errorDerivative = (error - previousError) / timeStep

    P = K_p * error
    I = K_i * errorIntegral
    D = K_d * errorDerivative

    print("P: " + str(P))
    print("I: " + str(I))
    print("D: " + str(D))

    U = P + I + D

    print("U: " + str(U))
    print(error)

    if 0 <= U <= 255:
        set_fan_rpm(U)
        set_servo_pos(U * (950 / 255) + 1350)
    elif U > 255:
        set_fan_rpm(255)
        set_servo_pos(2300)
    else:
        set_fan_rpm(0)
        set_servo_pos(1350)

        # if error is positive do some function of fann speed and servo position of

    previousError = error
    #time.sleep(timeStep)
    return (P,I,D,U,error, enclosureTemp, setTemp)

# Initialize communication with TMP102

# This function is called periodically from FuncAnimation
def animate(i, xs, ys):

    # Read temperature (Celsius) from TMP102
    # Add x and y to lists

    xs.append(dt.datetime.now().strftime('%H:%M:%S.%f'))
    P, I, D, U, error, enclosureTemp, setTemp = PID()
    ys.append(enclosureTemp)

    # Limit x and y lists to 20 items
    xs = xs[-20:]
    ys = ys[-20:]

    # Draw x and y lists
    ax.clear()
    ax.plot(xs, ys)

    # Format plot
    plt.xticks(rotation=45, ha='right')
    plt.subplots_adjust(bottom=0.30)
    plt.title('TMP102 Temperature over Time')
    plt.ylabel('Temperature (deg C)')

# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys), interval=timeStep)
plt.show()