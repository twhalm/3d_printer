from guizero import App, Text, TextBox, PushButton, Slider

def say_my_name():
    welcome_message.value = my_name.value
    
def change_text_size(slider_value):
    welcome_message.size = slider_value

app = App(title="Hello world", bg="black")

welcome_message = Text(app, text="Welcome to the jungle", size=40, font = "Times New Roman", color="white")

my_name = TextBox(app)

update_text = PushButton(app, command=say_my_name, text="Display My Name")

text_size = Slider(app, command=change_text_size, start=10, end=80)

update_text.bg = "white"
app.display()