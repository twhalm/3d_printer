import pigpio
import time


pin = 12
frequency = 25000
dutycycle = 0

pi = pigpio.pi()
pi.set_mode(pin, pigpio.OUTPUT)

pi.set_PWM_frequency(pin, frequency)
#pi.set_PWM_dutycycle(pin, dutycycle)

try:

    while True:
        for dc in range(0,255,5):
            pi.set_PWM_dutycycle(pin, dc)
            time.sleep(.1)
            print(dc)
        for dc in range(250,0,-5):
            pi.set_PWM_dutycycle(pin, dc)
            time.sleep(.1)
            print(dc)
except KeyboardInterrupt:
    pi.stop()




