
import time
import datetime as dt
#import matplotlib.pyplot as plt
#import matplotlib.animation as animation
# from PIL import ImageTk, Image
import pigpio


'''
i = 1


class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master, bg="#9E9E9E", cursor='none')
        self.master = master
        self.init_window()
        self.status = True
        self.temp_control()


    def init_window(self):
        self.pack(fill=BOTH, expand=1)
        self.power = PhotoImage(file="power54w.png")
        self.bulb = PhotoImage(file="lightbulb54.png")

        # titleLabel = Label(self, text = "ENCLOSURE CONTROL", font=helvbig, bg="white")
        # titleLabel.place(x=20,y=10)

        set_temp_label = Label(self, text="Set Temp", font=helvbig, fg="white")
        set_temp_label.config(bg="#9E9E9E")
        set_temp_label.place(x=530, y=10)

        actual_temp_label = Label(self, text="Actual Temp", font=helvbig, fg="white")
        actual_temp_label.config(bg="#9E9E9E")
        actual_temp_label.place(x=510, y=250)

        main_title = Label(self, text="ENCLOSURE CONTROL", font=helvbig, fg="white", relief=FLAT)
        main_title.config(bg="#424242", bd=0, activebackground="#424242", highlightbackground="#424242",
                          highlightthickness=0)
        main_title.place(x=0, y=0, width=500, height=60)

        pla_button = Button(self, text="PLA", font=helvbig, fg="white", relief=FLAT, command=self.PLA)
        pla_button.config(bg="#424242", bd=0, activebackground="white", highlightbackground="gray90",
                          highlightthickness=0)
        pla_button.place(x=0, y=60, width=250, height=140)

        abs_button = Button(self, text="ABS", font=helvbig, fg="white", relief=FLAT, command=self.ABS)
        abs_button.config(bg="#424242", bd=0, activebackground="white", highlightbackground="gray90",
                          highlightthickness=0)
        abs_button.place(x=0, y=200, width=250, height=140)

        power_button = Button(self, font=helvbig, fg="white", relief=FLAT, command=self.client_exit)
        power_button.config(bg="#424242", bd=0, activebackground="white", highlightbackground="gray90",
                            highlightthickness=0, image=self.power)
        power_button.place(x=0, y=340, width=250, height=140)

        petg_button = Button(self, text="PETG", font=helvbig, fg="white", relief=FLAT, command=self.PETG)
        petg_button.config(bg="#424242", bd=0, activebackground="white", highlightbackground="gray90",
                           highlightthickness=0)
        petg_button.place(x=250, y=60, width=250, height=140)

        other_button = Button(self, text="Other", font=helvbig, fg="white", relief=FLAT, command=self.PLA)
        other_button.config(bg="#424242", bd=0, activebackground="white", highlightbackground="gray90",
                            highlightthickness=0)
        other_button.place(x=250, y=200, width=250, height=140)

        light_button = Button(self, text="count", font=helvbig, fg="white", relief=FLAT, command=self.light_switch)
        light_button.config(bg="#424242", bd=0, activebackground="#424242", highlightbackground="gray90",
                            highlightthickness=0, image=self.bulb)
        light_button.place(x=250, y=340, width=250, height=140)

        temp_slider = Scale(self, variable=set_temp, from_=150, to=60, length=440, width=60, showvalue=0, bg="#424242",
                            troughcolor="#9E9E9E", relief=FLAT, highlightbackground="#424242")
        temp_slider.place(x=720, y=20)

        set_temp_label = Label(self, textvariable=set_temp, font=helvgiant, fg="white", bg="#9E9E9E")
        # set_temp.config(
        set_temp_label.place(x=530, y=80)

        enclosure_temp_label = Label(self, text=100, textvariable=enclosure_temp, font=helvgiant, fg="white", bg="#9E9E9E")
        enclosure_temp_label.place(x=530, y=320)

    def count_test(self):
        global i
        i = i + 1
        print(i)
        self.after(100, self.count_test)
    


    def temp_control(self):

        temp_diff = enclosure_temp.get()- set_temp.get()
        #something for ambient temp. if enclosure temp is <= ambient
        if temp_diff >= 30:
            set_fan_rpm(255)
            set_servo_pos(2300)
        elif temp_diff >= 20:
            set_fan_rpm(200)
            set_servo_pos(2300)
        elif temp_diff >= 10:
            set_fan_rpm(150)
            set_servo_pos(2300)
        elif temp_diff >= 5:
            set_fan_rpm(120)
            set_servo_pos(1750)
        elif 5>temp_diff>=0:
            set_fan_rpm(20)
            set_servo_pos(1400)
        else:
            set_fan_rpm(0)
            set_servo_pos(1350)
        self.after(5000, self.temp_control)
        
        
    def ABS(self):
        set_temp.set(120)
        
    def PLA(self):
        get_temp_amb=int(temp_calc(amb_tmp))
        set_temp.set(get_temp_amb)

    def PETG(self):
        set_temp.set(100)
'''

    # def set_temp(self, val):
    #     temperature.set("Temp {}".format(val))
    #     tempLabel.config(text =t)
    # def set_fan_rpm(dc):
    # pi.set_PWM_frequency(pin, frequency)
    # pi.set_PWM_dutycycle(pin, dc)

def client_exit():
    set_fan_rpm(0)
    set_servo_pos(1350)
    #exit i2c and gpio
    pi.i2c_close(enc_tmp)
    pi.i2c_close(amb_tmp)
    pi.stop()
    exit()


def set_fan_rpm(dc):
    pi.set_PWM_frequency(fan_pin, fan_frequency)
    # duty cycle range is 0-255
    pi.set_PWM_dutycycle(fan_pin, dc)


def temp_calc(a):
    # calculation of the temperature based on
    # the first word and the first byte
    
    byte0 = pi.i2c_read_byte_data(a, 0)
    word0 = pi.i2c_read_word_data(a, 0)
    l4b = (word0 & 0b1111000000000000) >> 12
    temperature = ((byte0 << 4) | l4b) * 0.0625
    if temperature > 0x7FF:
        temperature = temperature - 4096
    temp_f = temperature * 9 / 5 + 32
    return temp_f


def set_servo_pos(pulsewidth):
    # Pulsewidth between 0 and 2500 out of bounds may damage the servo
    #Full Close = 1350 open is 2300
    pi.set_servo_pulsewidth(servo_pin, pulsewidth)
    pi.set_servo_pulsewidth(servo_pin_2, pulsewidth)
    



# information needed for fan control
fan_pin = 12
fan_frequency = 25000
fan_dutycycle = 0

# information for servo control
servo_pin = 19
servo_frequency = 50
servo_dutycycle = 0

servo_pin_2 = 26

pi = pigpio.pi()
pi.set_mode(fan_pin, pigpio.OUTPUT)
pi.set_mode(servo_pin, pigpio.OUTPUT)
pi.set_mode(servo_pin_2, pigpio.OUTPUT)

# initialize the fan and set the speed(dutycycle) to zero
pi.set_PWM_frequency(fan_pin, fan_frequency)
pi.set_PWM_dutycycle(fan_pin, fan_dutycycle)

# initialize the servo and set it to default position
pi.set_PWM_frequency(servo_pin, servo_frequency)
pi.set_servo_pulsewidth(servo_pin, 0)

pi.set_PWM_frequency(servo_pin_2, servo_frequency)
pi.set_servo_pulsewidth(servo_pin_2, 0)

# configuration for the temperature sensors
# i2c bus of the Raspberry Pi 3
i2c_bus = 1
# TMP 102 address on the i2c bus
addr1 = 0x49
addr2 = 0x48
enc_tmp = pi.i2c_open(i2c_bus, addr1, 0)
amb_tmp = pi.i2c_open(i2c_bus, addr2, 0)




#
#set_temp = IntVar()
#e#nclosure_temp = IntVar()





K_p = 10
K_i = 1
K_d = 1

setTemp = 80
timeStep = .1000 #milliseconds
I_int = 0

def integrate (i,e1, e2, ts):

    area = ts * (e2 + e1) / 2
    
    integral = i + area
    
    return integral

previousError = 1
integral = 1
slope = 1
fanSpeed = 0
errorIntegral = 0
#client_exit()
reachedTemp = 0

while True:

    try:
        #enc_tmp = pi.i2c_open(i2c_bus, addr1, 0)
        #amb_tmp = pi.i2c_open(i2c_bus, addr2, 0)
        enclosureTemp=int(temp_calc(enc_tmp))
        print (enclosureTemp)
        ambientTemp = int(temp_calc(amb_tmp))
        print (ambientTemp)
        setTemp = ambientTemp+10
        error = enclosureTemp - setTemp
        
        if error == 0 and reachedTemp == 0:
            reachedTemp = 1 

        #if fanSpeed < 255:
        errorIntegral = integrate(errorIntegral, previousError, error, timeStep)
        
        if reachedTemp == 1:
            #clear error once we reach the temp for the first time
            errorIntegral = 0
            reachedTemp = 2
            
        
        errorDerivative = (error - previousError) / timeStep
                                  
        P = K_p * error  
        I = K_i * errorIntegral
        D = K_d * errorDerivative
        
        print ("P: " + str(P))
        print ("I: " + str(I))
        print ("D: " + str(D))
        
        U = P + I + D
        
        print ("U: " + str(U))
        print (error)
        
        if 0 <= U <= 255:
            set_fan_rpm(U)
            set_servo_pos(U * (950/255) +1350)
        elif U > 255:
            set_fan_rpm(255)
            set_servo_pos(2300)
        else:
            set_fan_rpm(0)
            set_servo_pos(1350)  
                          
        
        #if error is positive do some function of fann speed and servo position of 
                                  
                                  
        previousError = error
        time.sleep(timeStep)
    except KeyboardInterrupt:
        break
'''
enclosureTemp=int(temp_calc(enc_tmp))
ambientTemp = int(temp_calc(amb_tmp))

print(enclosureTemp)
print(ambientTemp)

print ("Opening Full and full Fan")
set_fan_rpm(255)
set_servo_pos(2300)
       
time.sleep(5)
'''
print ("closing And fan off")
set_fan_rpm(0)
set_servo_pos(1350)
client_exit()