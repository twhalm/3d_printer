import pigpio
import time

pin = 12
frequency = 25000
dutycycle = 0

pi = pigpio.pi()
pi.set_mode(pin, pigpio.OUTPUT)

pi.set_PWM_frequency(pin, frequency)
pi.set_PWM_dutycycle(pin, dutycycle)

def set_fan_rpm(dc):
    pi.set_PWM_frequency(pin, frequency)
    pi.set_PWM_dutycycle(pin, dc)

def tmp102_reading(byte0, word0):
 #calculation of the temperature based on 
 #the first word and the first byte
 
 # !!! not tested for negative temperatures !!!!
 #last 4 bits of the word0
    l4b = (word0 & 0b1111000000000000)>>12
    temperature = ((byte0<<4) | l4b) * 0.0625
    if temperature > 0x7FF:
        temperature = temperature-4096;
    tempF = temperature * 9/5+32    
        
    return tempF
 
 
#i2c bus of the Raspberry Pi 3
i2c_bus = 1
#TMP 102 address on the i2c bus
addr = 0x48
#dev_pi = pigpio.pi()
dev_tmp = pi.i2c_open(i2c_bus, addr, 0)

try:
    while True:
        t_byte = pi.i2c_read_byte_data(dev_tmp, 0)
        t_word = pi.i2c_read_word_data(dev_tmp, 0)
        t = tmp102_reading(t_byte, t_word)
        print(' Temperature: {} F'.format(t))
        time.sleep(.1)
        if t > 75.0:
            set_fan_rpm(100)
        else:
            set_fan_rpm(0)
except KeyboardInterrupt:
    pass
print('Exiting the loop');
r = pi.i2c_close(dev_tmp)
pi.stop()
