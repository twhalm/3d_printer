import pigpio
import time


pin = 19
frequency = 50
dutycycle = 0

pi = pigpio.pi()
pi.set_mode(pin, pigpio.OUTPUT)

pi.set_PWM_frequency(pin, frequency)
#pi.set_PWM_dutycycle(pin, dutycycle)

try:
#    pi.set_servo_pulsewidth(pin, 0)    # off
 #   time.sleep(1)
  #  pi.set_servo_pulsewidth(pin, 500)
   # time.sleep(1)
#    pi.set_servo_pulsewidth(pin, 1000) # safe anti-clockwise
#    time.sleep(1)
    pi.set_servo_pulsewidth(pin, 1350) # centre
    time.sleep(1)
    pi.set_servo_pulsewidth(pin, 2000)
    time.sleep(1)
    pi.set_servo_pulsewidth(pin, 2300)
    time.sleep(1)
    pi.set_servo_pulsewidth(pin, 1350)
    pi.stop()
except KeyboardInterrupt:
    pi.stop()





